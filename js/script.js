$(function(){
  $('#heading').data('size','big');
});

$(window).scroll(function(){
  if($(document).scrollTop() > 200)
{
    if($('#heading').data('size') == 'big')
    {
        $('#heading').data('size','small');
        $('#heading').animate({
            height:'50px'
        },600);
        $('#nav').animate({
        	top:'20px'
        },600);
    }
}
else
  {
    if($('#heading').data('size') == 'small')
      {
        $('#heading').data('size','big');
        $('#heading').stop().animate({
            height:'80px'
        },600);
      }  
  }
});

function prevPic(){
	var currentActiveImage=$(".ImgShow");
	var previousActiveImage=currentActiveImage.prev();

	if(previousActiveImage.length==0){
		previousActiveImage=$("#pic-inner img").last();
	}

	currentActiveImage.removeClass("ImgShow").addClass("ImgHidden");
	previousActiveImage.removeClass("ImgHidden").addClass("ImgShow");
}

function nextPic(){
	var currentActiveImage=$(".ImgShow");
	var nextActiveImage=currentActiveImage.next();

	if(nextActiveImage.length==0){
		nextActiveImage=$("#pic-inner img").first();
	}

	currentActiveImage.removeClass("ImgShow").addClass("ImgHidden");
	nextActiveImage.removeClass("ImgHidden").addClass("ImgShow");
}

function close(){
	document.getElementById("modal").style.display="none";
}

function modal1(){
	document.getElementById("d1").style.display="block";
	document.getElementById("d2").style.display="none";
	document.getElementById("d3").style.display="none";
	document.getElementById("modal").style.display="block";
}

function modal2(){
	document.getElementById("d2").style.display="block";
	document.getElementById("d1").style.display="none";
	document.getElementById("d3").style.display="none";
	document.getElementById("modal").style.display="block";
}

function modal3(){
	document.getElementById("d3").style.display="block";
	document.getElementById("d1").style.display="none";
	document.getElementById("d2").style.display="none";
	document.getElementById("modal").style.display="block";
}

function homeClick(){
	$(".currPart").removeClass("currPart");
	$("#h").addClass("currPart");
}

function backgroundClick(){
	$(".currPart").removeClass("currPart");
	$("#bg").addClass("currPart");
}

function contactClick(){
	$(".currPart").removeClass("currPart");
	$("#cont").addClass("currPart");
}

window.onload=function(){
	document.getElementById("h").onclick=homeClick;
	document.getElementById("bg").onclick=backgroundClick;
	document.getElementById("cont").onclick=contactClick;
	document.getElementById("prev").onclick=prevPic;
	document.getElementById("next").onclick=nextPic;
	document.getElementById("close").onclick=close;
	document.getElementById("dog1").onclick=modal1;
	document.getElementById("dog2").onclick=modal2;
	document.getElementById("dog3").onclick=modal3;
}